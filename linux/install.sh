#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root, use sudo "$0" instead" 1>&2
  exit 1
fi

export DEBIAN_FRONTEND="noninteractive"

echo 'Install Apache2'
apt-get install apache2 -y > /dev/null

echo 'Install SQLite'
apt-get install sqlite -y > /dev/null

echo 'Install MySQL'
apt-get install mysql-server -y > /dev/null

echo 'Install PostGreSQL'
apt-get install postgresql postgresql-contrib -y > /dev/null

echo 'Install PHP5.6'
apt-get install php5.6-dev php5.6-sqlite3 php5.6-mysql php5.6-pgsql php5.6-xdebug php5.6-gettext php5.6-mbstring php5.6-xml libapache2-mod-php5.6 -y > /dev/null

echo 'Install PHP7.0'
apt-get install php7.0-dev php7.0-sqlite3 php7.0-mysql php7.0-pgsql php7.0-xdebug php7.0-gettext php7.0-mbstring php7.0-xml libapache2-mod-php7.0 -y > /dev/null

echo 'Install PHP7.1'
apt-get install php7.1-dev php7.1-sqlite3 php7.1-mysql php7.1-pgsql php7.1-xdebug php7.1-gettext php7.1-mbstring php7.1-xml libapache2-mod-php7.1 -y > /dev/null

echo 'Install PHP7.2'
apt-get install php7.2-dev php7.2-sqlite3 php7.2-mysql php7.2-pgsql php7.2-xdebug php7.2-gettext php7.2-mbstring php7.2-xml libapache2-mod-php7.2 -y > /dev/null

echo 'Install PHPUnit (5, 6 & 7)'
wget -O /usr/local/bin/phpunit-5 https://phar.phpunit.de/phpunit-5.phar > /dev/null
wget -O /usr/local/bin/phpunit-6 https://phar.phpunit.de/phpunit-6.phar > /dev/null
wget -O /usr/local/bin/phpunit-7 https://phar.phpunit.de/phpunit-7.phar > /dev/null
chmod ugo+x /usr/local/bin/phpunit-* > /dev/null

echo 'Install composer'
wget -O ~/composer-install.php https://getcomposer.org/installer > /dev/null
php ~/composer-install.php --install-dir=/usr/local/bin --filename=composer > /dev/null

echo 'Install PHP CodeSniffer'
wget -O /usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar > /dev/null
wget -O /usr/local/bin/phpcbf https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar > /dev/null
chmod ugo+x /usr/local/bin/phpcs /usr/local/bin/phpcbf

echo 'Install shortcuts'
cp linux/php* /usr/local/bin
chmod +x /usr/local/bin/php*

echo 'Replace home page'
rm /var/www/html/index.*
cp src/index.php /var/www/html
chmod +r /var/www/html/index.php
