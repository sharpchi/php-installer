<?php

/**
 * Default index page
 *
 * PHP Version 5.6, 7.0, 7.1, 7.2
 *
 * @category  Test
 * @package   PHPInstaller
 * @author    Nhu-Hoai Robert VO <nhuhoai.vo@franicflow.ch>
 * @copyright 2018 FRANIC Flow Sàrl
 * @license   No license
 * @version   GIT: 2.1.0
 * @link      https://gitlab.com/franicflowsarl/php-installer
 * @since     2.1.0
 */

$html = <<< EOFILE
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>You rock! You can use multiple PHP versions now!</title>
  </head>
  <body>
    <p><b>You rock! You can use multiple PHP versions now!</b></p>

    <p>What's included:</p>
    <ul>
      <li><a href="http://php.net/">PHP</a></li>
      <ul>
        <li>5.6</li>
        <li>7.0</li>
        <li>7.1</li>
        <li>7.2</li>
      </ul>
      <li>MySQL server</li>
      <li>PostGreSQL server</li>
      <li>SQLite package</li>
      <li><a href="http://pear.php.net/package/PHP_CodeSniffer">PHP_CodeSniffer</a></li>
      <li><a href="https://phpunit.de/">PHPUnit</a></li>
      <li><a href="https://getcomposer.org/">Composer</a></li>
    </ul>

    <p>Change easily the PHP version with those commands:</p>

    <ul>
      <li>sudo php5.6</li>
      <li>sudo php7.0</li>
      <li>sudo php7.1</li>
      <li>sudo php7.2</li>
    </ul>

    <p>
      For more details about this installer, please visit the
      <a href="https://gitlab.com/franicflowsarl/php-installer">GitLab repository</a>.
    </p>

    <p>
      Thanks for using our PHP installer! Please visit our
      <a href="https://github.com/FranicFlow">GitHub</a> or
      <a href="https://gitlab.com/franicflowsarl/">GitLab</a> for more scripts!

    <p>Your current PHP settings:</p>
  </body>
</htmL>
EOFILE;

print $html;

phpinfo();

?>
