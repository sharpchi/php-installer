[project]: https://gitlab.com/franicflowsarl/php-installer
[version]: https://img.shields.io/badge/version-0.2.1-blue.svg
[build]: https://gitlab.com/franicflowsarl/php-installer/badges/master/pipeline.svg
[semver]: https://semver.org/
[gitlab/nhuhoai]: https://gitlab.com/nhuhoai

# PHP Installer

[![][version]][project]
[![][build]][project]

Install multiple PHP versions easily

## Release note

#### Fixes

-   Missing dependencies for PHPCS (add php xml package)

#### Additions

-   One line script install
-   Test phpcs and phpunit commands with fake data
-   Custom home page

Please read [CHANGELOG](CHANGELOG.md) for the complete history.

## Getting started

### Prerequisites

Debian (tested on Rapsbian) or Ubuntu distribution

### Installing

#### One line script

For Debian:

```
curl -L https://gitlab.com/franicflowsarl/php-installer/raw/master/install_debian.sh | sudo bash
```

For Ubuntu:

```
curl -L https://gitlab.com/franicflowsarl/php-installer/raw/master/install_ubuntu.sh | sudo bash
```

#### Using Git repository

1.  If you do not have git, please installe the package.
```
sudo apt-get install git
```
2.  Clone this project
```
git clone https://gitlab.com/franicflowsarl/php-installer.git
cd php-installer
```
3.  Run linux/debian.sh or linux/ubuntu.sh
```
./linux/debian.sh # use this one for Raspbian
./linux/ubuntu.sh
```

### Configuring

#### Changing PHP version
```
sudo php5.6
sudo php7.0
sudo php7.1
sudo php7.2
```

Config file for PHP environment ```/etc/php/#.#/apache2/php.ini```

#### MySQL Server

**By default, every database services are starting at startup.**

```
# Please exec this command after a fresh install
sudo mysql_secure_installation

# Enable service
sudo systemctl enable mysql

# Disable service
sudo systemctl disable mysql
```

#### PostGreSQL

**By default, every database services are starting at startup.**

```
# Enable service
sudo systemctl enable postgresql

# Disable service
sudo systemctl disable postgresql
```

#### Apache2

Main config file for Apache2: ```/etc/apache2/apache2.conf```

```
# Enable service
sudo systemctl enable apache2

# Disable service
sudo systemctl disable apache2
```

### Running the tests

You need Docker and Gitlab Runner on your computer.

```
# job_param can be found into .gitlab-ci.yml
gitlab-runner exec docker job_param
```

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

This project use the [SemVer][semver] for versioning.

## Authors &amp; contributors

-   Nhu-Hoai Robert VO - Author - [nhuhoai][gitlab/nhuhoai]

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE.md) file for details.

### Third-parties

-   [PHP](http://php.net/) ([license](http://php.net/copyright.php))
-   [PHP packages](https://deb.sury.org/), THANKS to Ondřej Surý!!!
-   [PHPUnit](https://phpunit.de/) ([license](https://github.com/sebastianbergmann/phpunit/blob/master/LICENSE))
-   [Composer](https://getcomposer.org/) ([license](https://github.com/composer/composer/blob/master/LICENSE))
-   [PHP_CodeSniffer](http://pear.php.net/package/PHP_CodeSniffer) ([license](https://github.com/squizlabs/PHP_CodeSniffer/blob/master/licence.txt))
